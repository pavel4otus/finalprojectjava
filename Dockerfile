FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app

ENV PORT 8000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ] 
